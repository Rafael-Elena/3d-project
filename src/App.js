import * as THREE from 'three';
import ft from './assets/texture_ft.bmp';
import dn from './assets/texture_dn.bmp';
import bk from './assets/texture_bk.bmp';
import lf from './assets/texture_lf.bmp';
import rt from './assets/texture_rt.bmp';
import up from './assets/texture_up.bmp';
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";

import './App.css';

const scene = new THREE.Scene();

const camera = new THREE.PerspectiveCamera(55, window.innerWidth/window.innerHeight, 45, 30000);
camera.position.set(-900, -200, -900);

const renderer = new THREE.WebGLRenderer({antialias:true});
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);
//
//let controls = new OrbitControls(camera);
//controls.addEventListener('change', renderer);

let materialArray = [];
let texture_ft = new THREE.TextureLoader().load(ft);
let texture_bk = new THREE.TextureLoader().load(bk);
let texture_dn = new THREE.TextureLoader().load(dn);
let texture_lf = new THREE.TextureLoader().load(lf);
let texture_up = new THREE.TextureLoader().load(up);
let texture_rt = new THREE.TextureLoader().load(rt);

materialArray.push(new THREE.MeshBasicMaterial({map: texture_ft}));
materialArray.push(new THREE.MeshBasicMaterial({map: texture_bk}));
materialArray.push(new THREE.MeshBasicMaterial({map: texture_dn}));
materialArray.push(new THREE.MeshBasicMaterial({map: texture_lf}));
materialArray.push(new THREE.MeshBasicMaterial({map: texture_up}));
materialArray.push(new THREE.MeshBasicMaterial({map: texture_rt}));

for(let i=0;i<6;i++)
  materialArray[i].side = THREE.BackSide;

let skyboxGeo = new THREE.BoxGeometry(10000, 10000, 10000,);
let skybox = new THREE.Mesh(skyboxGeo, materialArray);
scene.add(skybox);

animate();

function animate() {
  renderer.render(scene,camera);
  requestAnimationFrame(animate);
}

function App() {


  return (
    <div className="App">
    </div>
  );

}

export default App;
